<?php 
//无需修改本页任何内容，除非你十分了解并足够把握
if($isoso=="1"){ $sjian="onBlur";}else{ $sjian="onKeyup";}
$tps=="y";
/*
************************************************************
作者：yujianyue (admin@12391.net) 
作者主页：http://soulide.cn/（搜立得）
php+MySql已有数据表通用搜索可增删改查 V20221111版
增：右上角进入;  删：可单删多删;
改：查询结果页灰底可改;需conn.php设置可改字段;
查：六种查询规则可切换(多字段选一等于、包含、开始、结尾；不选查多字段之一等于、包含);
查询结果分页，可以自定义每页数量，最大页数；可隐藏指定列，可将网址字段显示为链接或图片;
适合自己有PHP网站环境及Mysql软件并熟练操作的细心用户
************************************************************
*/
$conn = mysqli_connect($dbhost.":".$dbport, $dbuser, $dbpass, $dbname);
if (!$conn) { die("连接失败: " . mysqli_connect_error()); }

function webalert($Key){
 $html="<script>\r\n";
 $html.="alert('".$Key."');\r\n";
 $html.="history.go(-1);\r\n";
 $html.="</script>";
 exit($html);
}
function webtable($Key){
 $html.="<table cellspacing=\"0\">\r\n";
 $html.='<tbody><tr>';
 $html.="<td data-label=\"查询提示\">$Key</td>";
 $html.='</tr></tbody>';
 $html.="</table>";
 exit($html);
}

//读取指定行
function getline($filename, $line){
  $n = 0;
  $handle = fopen($filename,'r');
  if ($handle) {
    while (!feof($handle)) {
        $n++;
        $out = fgets($handle, 4096);
        if($line==$n) break;
    }
    fclose($handle);
  }
  if($line==$n) return $out;
  return false;
}

?>