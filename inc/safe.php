<?php
//这里可以增加mysql防注入等安全代码
/*
************************************************************
作者：yujianyue (admin@12391.net) 
作者主页：http://soulide.cn/ （搜立得）
php+MySql已有数据表通用搜索可增删改查 V20221111版
增：右上角进入;  删：可单删多删;
改：查询结果页灰底可改;需conn.php设置可改字段;
查：六种查询规则可切换(多字段选一等于、包含、开始、结尾；不选查多字段之一等于、包含);
查询结果分页，可以自定义每页数量，最大页数；可隐藏指定列，可将网址字段显示为链接或图片;
适合自己有PHP网站环境及Mysql软件并熟练操作的细心用户
************************************************************
*/
//get拦截规则
$getfilter = "\\<.+javascript:window\\[.{1}\\\\x|<.*=(&#\\d+?;?)+?>|<.*(data|src)=data:text\\/html.*>|\\b(alert\\(|confirm\\(|expression\\(|prompt\\(|benchmark\s*?\(.*\)|sleep\s*?\(.*\)|load_file\s*?\\()|<[a-z]+?\\b[^>]*?\\bon([a-z]{4,})\s*?=|^\\+\\/v(8|9)|\\b(and|or)\\b\\s*?([\\(\\)'\"\\d]+?=[\\(\\)'\"\\d]+?|[\\(\\)'\"a-zA-Z]+?=[\\(\\)'\"a-zA-Z]+?|>|<|\s+?[\\w]+?\\s+?\\bin\\b\\s*?\(|\\blike\\b\\s+?[\"'])|\\/\\*.*\\*\\/|<\\s*script\\b|\\bEXEC\\b|UNION.+?SELECT\s*(\(.+\)\s*|@{1,2}.+?\s*|\s+?.+?|(`|'|\").*?(`|'|\")\s*)|UPDATE\s*(\(.+\)\s*|@{1,2}.+?\s*|\s+?.+?|(`|'|\").*?(`|'|\")\s*)SET|INSERT\\s+INTO.+?VALUES|(SELECT|DELETE)@{0,2}(\\(.+\\)|\\s+?.+?\\s+?|(`|'|\").*?(`|'|\"))FROM(\\(.+\\)|\\s+?.+?|(`|'|\").*?(`|'|\"))|(CREATE|ALTER|DROP|TRUNCATE)\\s+(TABLE|DATABASE)";

function webscan_arr_foreach($arr) {
static $str;
if (!is_array($arr)) {
return $arr;
}
foreach ($arr as $key => $val ) {
if (is_array($val)) {
webscan_arr_foreach($val);
} else {
$str[] = $val;
}
}
return implode($str);
}

function webscan_pape(){
$pape='<style type="text/css">h1,p{text-align:center;}';
$pape.='h1{line-height:150%;font-size:38px;color:red;}';
$pape.='p{line-height:120%;font-size:18px;color:blue;}</style>';
$pape.="<h1>欢迎光临，手下留情</h1>";
$pape.="<p>输入内容存在危险字符，你的操作被拦截</p>";
echo $pape;
exit();
}

function webscan_StopAttack($StrFiltKey,$StrFiltValue,$ArrFiltReq,$method) {
$StrFiltValue=webscan_arr_foreach($StrFiltValue);
if (preg_match("/".$ArrFiltReq."/is",$StrFiltValue)==1){
webscan_pape();
}
if (preg_match("/".$ArrFiltReq."/is",$StrFiltKey)==1){
webscan_pape();
}
}

if ($webscan_get) {
foreach($_GET as $key=>$value) {
webscan_StopAttack($key,$value,$getfilter,"GET");
}
}


?>